package com.ucbcba.proyecto.proyecto.Repositories;

import com.ucbcba.proyecto.proyecto.Entities.Ciudad;
import org.springframework.data.jpa.repository.JpaRepository;


import javax.transaction.Transactional;

@Transactional
public interface CiudadRepository extends JpaRepository<Ciudad, Integer> {
}